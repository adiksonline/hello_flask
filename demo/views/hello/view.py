from flask import url_for, abort, session, escape, request, redirect, render_template, g, flash
from flask import Blueprint, jsonify, make_response, json, current_app
from demo.db.models import User
from demo.db import db_session

app = Blueprint("hello", "hello")


@app.route("/")
def hello():
    #print url_for("static", filename = "interesting.css")
    #print url_for(".hello")
    return render_template("home.html", user = g.user)

@app.route("/login/", methods = ["GET", "POST"])
def login():
    if g.user: return redirect(url_for(".hello"))
    if request.method == "POST":
        username = request.form["username"]
        current_app.logger.debug("Got username from request: {0}".format(username))
        user = User.query.filter(User.username == username).first()
        if user == None:
            current_app.logger.debug("User {0} does not exist in the database".format(username))
            flash("Invalid username, please try again")
        else:
            current_app.logger.debug("User {0} found. Adding to seesion :)".format(user))
            session["username"] = user.username
            return redirect(url_for(".hello"))
    return render_template("login.html")

@app.route("/signup/", methods = ["GET", "POST"])
def register():
    if g.user: return redirect(url_for(".hello"))
    if request.method == "POST":
        username = request.form.get("username", None)
        name = request.form.get("fullname", None)
        bio = request.form.get("bio", None)
        if username and name:
            user = User(username, name)
            user.setBio(bio)
            try:
                db_session.add(user)
                db_session.commit()
                session["username"] = username
                flash("Success")
                return redirect(url_for(".hello"))
            except:
                flash("Username already exists. Please try again")
                pass
    return render_template("signup.html")

@app.route("/logout/")
def logout():
    session.pop("username", None)
    flash("You are now logged out!")
    return redirect(url_for(".hello"))


########################################################################

@app.route("/test/")
def testing():
    l = ranger()
    return render_template("home.html", lis=l)

@app.route("/api/test/")
def apitest():
    resp = make_response(json.dumps(dict(result=ranger()), separators=(",", ":")))
    resp.mimetype = "text/json"
    current_app.logger.debug("shit happened")
    return resp

def ranger():
    return range(6)

