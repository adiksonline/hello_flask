
from flask import Flask, g, session
import os

app = Flask("demo")
fname = os.getcwd() + "/demo.db"
SQLALCHEMY_DATABASE_URI = "sqlite:///" + fname
DEBUG = True
SECRET_KEY = "development key"
#app.secret_key = "internationalism"
app.config.from_object(__name__)

from views import hello
from db.models import User
from db import db_session, init_db

if not os.path.exists(fname):
    init_db()

app.register_blueprint(hello.blueprint)

@app.errorhandler(404)
def pageNotFound(error):
    return "crap happens. page not found!", 404

@app.errorhandler(500)
def serverError(error):
    #active only when debug is false
    return "An awesome error happened from the server!", 500

@app.before_request
def setupUser():
    g.user = User.query.filter_by(username = session["username"]).first() if "username" in session else None

@app.teardown_request
def cleanUp(error):
    db_session.remove()


