from sqlalchemy import String, Column, Integer

from . import Base

class User(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key = True)
    name = Column(String(50))
    username = Column(String(30), unique = True)
    bio = Column(String(150))

    def __init__(self, username, name):
        self.name = name
        self.username = username

    def setBio(self, bio):
        self.bio = bio

    def getBio(self):
        return self.bio

    def __repr__(self):
        return "<User ({0})>".format(self.username)
