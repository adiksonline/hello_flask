from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.ext.declarative import declarative_base
from demo import app

engine = create_engine(app.config["SQLALCHEMY_DATABASE_URI"])
db_session = scoped_session(sessionmaker(bind = engine))

Base = declarative_base(bind = engine)
Base.query = db_session.query_property()


def init_db():
    # import all the models that make use of Base here
    # so that they can be properly handled by the create_all() method
    import models
    Base.metadata.create_all()


def drop_db():
    import models
    Base.metadata.drop_all()
