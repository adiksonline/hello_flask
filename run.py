import os
import sys

#sys.path.append(os.path.join(os.path.abspath('.'), 'lib'))
from demo import app


if __name__ == "__main__":
    print app.url_map

    #app.run(host = "0.0.0.0")


    """from tornado.httpserver import HTTPServer
    from tornado.wsgi import WSGIContainer
    from tornado.ioloop import IOLoop

    http_server = HTTPServer(WSGIContainer(app))
    http_server.listen(5000)
    IOLoop.instance().start()"""

    from gevent.wsgi import WSGIServer
    from gevent import monkey
    monkey.patch_all()
    http_server = WSGIServer(("0.0.0.0", 5000), app) #, keyfile="server.key", certfile="server.crt")
    http_server.serve_forever()